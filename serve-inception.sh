#!/usr/bin/env bash

set -ex

source env.sh

cd ${APP_NAME}

MODEL_COMPONENT=serveInception
MODEL_NAME=inception
MODEL_PATH=gs://kubeflow-models/inception
ks generate tf-serving ${MODEL_COMPONENT} --name=${MODEL_NAME}
ks param set ${MODEL_COMPONENT} modelPath ${MODEL_PATH}

ks apply ${KF_ENV} -c ${MODEL_COMPONENT}

kubectl get svc inception -n=${NAMESPACE}