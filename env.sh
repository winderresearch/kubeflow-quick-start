#!/usr/bin/env bash

GCLOUD=gcloud
CLUSTER_NAME=kubeflow-quick-start
CLUSTER_NODES=1
MACHINE_TYPE=n1-standard-4
ZONE=europe-west1-b
NAMESPACE=kubeflow
APP_NAME=my-kubeflow
KF_ENV=cloud
# Which version of Kubeflow to use
# For a list of releases refer to:
# https://github.com/kubeflow/kubeflow/releases
VERSION=v0.1.2