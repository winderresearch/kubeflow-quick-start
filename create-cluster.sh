#!/usr/bin/env bash

set -ex

source env.sh

if ! ${GCLOUD} container clusters list 2> /dev/null | grep -q ${CLUSTER_NAME} ; then
    ${GCLOUD} container clusters create ${CLUSTER_NAME} \
        --machine-type ${MACHINE_TYPE} \
        --num-nodes ${CLUSTER_NODES} \
        --zone ${ZONE}
    ${GCLOUD} container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE}
fi

${GCLOUD} container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE}

kubectl get node

