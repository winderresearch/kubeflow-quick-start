#!/usr/bin/env bash

set -ex

source env.sh

CNN_JOB_NAME=mycnnjob

cd ${APP_NAME}

ks registry add kubeflow-git github.com/kubeflow/kubeflow/tree/${VERSION}/kubeflow
ks pkg install kubeflow-git/examples

ks generate tf-job-simple ${CNN_JOB_NAME} --name=${CNN_JOB_NAME}

ks apply ${KF_ENV} -c ${CNN_JOB_NAME}
